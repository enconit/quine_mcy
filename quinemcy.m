function quinemcy
N=input('Ingrese Nro. de Variables:  ');
m=input('Ingrese minit�rminos entre corchetes y separados por espacio:  ');
%   N: Numero de Elementos   (<=10)
%   m: lista de miniterminos


if max(m) >= 2^N,       %Error si se introduce más minterms de los posibles
    disp('Error, N es pequeño');
    return;
end

variables = 'ABCDEFGHIJ';
m = unique(sort(m));    %Ordena los minterms y evita repeticiones

mLen = length(m);       %Obtiene Nro de minterms


if mLen  == 2^N,
	resul = '1';
	return;
end


combinacion = zeros(mLen , 1); %Vector para indicar si dos expresiones se combinaron
nuevobinstr = [];
final = [];

binstr = [dec2bin(m, N)];       %Matriz de minterms en binario
funcion=[];

for k = 1:size(binstr, 1),      %Bucle para obtener funcion en forma canonica
    vec = binstr(k,:);
    for p = 1:N,
        
        if vec(p) == '1',       %Compara bit a bit
            funcion = [funcion variables(p)];
        else if vec(p) == '0',
            funcion = [funcion variables(p) ''''];
            end
        end
    end
    if k ~= size(binstr, 1),    %Si no se llego al ultimo minterm
        funcion = [funcion ' + '];
    end
end
fprintf('\t La funcion booleana es :  \n  F = %s \n \n',funcion);

while 1
    cuenta = 0;
    Len = size(binstr, 1);
    for p = 1:(Len - 1),    %Analiza cada minterms con los restantes
        for q = (p + 1):Len,
            notEqual = (binstr(p,:) ~= binstr(q,:));
            if sum(notEqual) == 1,  %Si difieren en 1 bit
                cuenta = cuenta + 1;%Indica que hubo combinacion de minterm p con q
                combinacion(p) = 1;
                combinacion(q) = 1;
                tmp = binstr(p,:);
                tmp(notEqual) = '-';
                nuevobinstr = [nuevobinstr; tmp]; %Guarda los combinados
            end
        end
    end
  
    for k = 1:Len,
        if combinacion(k) == 0, %El vector final son lo que ya no pueden combinarse
            final = [final; binstr(k,:)];
        end
    end
    if cuenta == 0,
        break;
    end
    combinacion = zeros(cuenta, 1); %Genera de nuevo, con las expresiones sin combinar
    binstr = nuevobinstr;
    binstr = unique(binstr, 'rows');    
 
    nuevobinstr = [];
end
final = unique(final, 'rows');     %Elimina terminos repetidos



resul = [];
for k = 1:size(final, 1),
    vec = final(k,:);
    for p = 1:N,
        if vec(p) == '-',
            continue;
        end
        if vec(p) == '1',
            resul = [resul variables(p)];
        else if vec(p) == '0',
            resul = [resul variables(p) ''''];
            end
        end
    end
    if k ~= size(final, 1),
        resul = [resul '+'];
    end
end
fprintf('\t Los implicantes primarios  son :  \n  F = %s \n',resul);



end
